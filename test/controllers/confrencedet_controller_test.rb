require 'test_helper'

class ConfrencedetControllerTest < ActionController::TestCase
  test "should get main" do
    get :main
    assert_response :success
  end

  test "should get speakers" do
    get :speakers
    assert_response :success
  end

  test "should get fees" do
    get :fees
    assert_response :success
  end

  test "should get presentation" do
    get :presentation
    assert_response :success
  end

  test "should get venue" do
    get :venue
    assert_response :success
  end

  test "should get sponsorship" do
    get :sponsorship
    assert_response :success
  end

  test "should get abstractdetails" do
    get :abstractdetails
    assert_response :success
  end

end
