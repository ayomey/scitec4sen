require 'test_helper'

class WelcomeControllerTest < ActionController::TestCase
  test "should get confrence" do
    get :confrence
    assert_response :success
  end

  test "should get contact" do
    get :contact
    assert_response :success
  end

  test "should get index" do
    get :index
    assert_response :success
  end

end
