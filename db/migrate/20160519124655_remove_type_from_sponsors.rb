class RemoveTypeFromSponsors < ActiveRecord::Migration
  def change
    remove_column :sponsors, :type
    add_column :sponsors, :choose, :string
    
  end
end
