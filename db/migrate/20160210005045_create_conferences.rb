class CreateConferences < ActiveRecord::Migration
  def change
    create_table :conferences do |t|
      t.string :organisation
      t.string :position
      t.string :address
      t.string :postcode
      t.string :city
      t.string :country
      t.bigint :telephone

      t.timestamps null: false
    end
  end
end
