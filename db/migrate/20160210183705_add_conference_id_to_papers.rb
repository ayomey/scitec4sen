class AddConferenceIdToPapers < ActiveRecord::Migration
  def change
    add_column :papers, :conference_id, :integer
  end
end
