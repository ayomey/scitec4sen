class CreateAbstracts < ActiveRecord::Migration
  def change
    create_table :abstracts do |t|
      t.string :title
      t.text :content
      t.string :status
      t.string :comment

      t.timestamps null: false
    end
  end
end
