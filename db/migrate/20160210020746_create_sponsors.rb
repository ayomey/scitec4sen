class CreateSponsors < ActiveRecord::Migration
  def change
    create_table :sponsors do |t|
      t.string :organisation
      t.text :address
      t.string :website
      t.bigint :telephone
      t.string :type
      t.text :comment

      t.timestamps null: false
    end
  end
end
