class MessagesController < ApplicationController
  def new
     @message = Message.new
  end
  
  def create
    @message = Message.new(message_params)

    if @message.valid?
      MessageMailer.message_me(@message).deliver_now

      redirect_to root_path, notice: "Thankyou for your message. We will get back to you soon"
    else
      render :new
    end
  end

  private

  def message_params
    params.require(:message).permit(:name, :email, :subject, :content)
  end
  
end
