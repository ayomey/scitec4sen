class Paper < ActiveRecord::Base
    belongs_to :user
    belongs_to :conference
    validates :organisation, :title, :content, :presence => true
end
