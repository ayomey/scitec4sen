class Conference < ActiveRecord::Base
    belongs_to :user
    has_many :papers
    validates :organisation, :position, :address, :city, :country, :postcode, :telephone, :presence => true
    
    
    def admin?
        
        admin :true
        
    end
end
