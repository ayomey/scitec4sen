class Sponsor < ActiveRecord::Base
    belongs_to :user
    validates :organisation, :telephone, :choose, :presence => true
end
