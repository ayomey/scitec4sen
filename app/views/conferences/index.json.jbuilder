json.array!(@conferences) do |conference|
  json.extract! conference, :id, :organisation, :position, :address, :postcode, :city, :country, :telephone
  json.url conference_url(conference, format: :json)
end
