json.array!(@sponsors) do |sponsor|
  json.extract! sponsor, :id, :organisation, :address, :website, :telephone, :type, :comment
  json.url sponsor_url(sponsor, format: :json)
end
